
$(document).ready(function() {
	$('#btnParse').click(function() {
		getChannelContent($('#targetUrl').val());
	});

	$('#btnParseUser').click(function() {
		parseUser($('#targetUserName').val());
	})
});
//==== 這裡的 apiKey 要改成自己申請的 apiKey =======
var apiKey = "";

var objChannel ;
var books;	//可以匯出的每一本書

var parseUser = function(userName) {

	var targetUrl = ["https://www.googleapis.com/youtube/v3/channels?forUsername=" , userName, 
						"&key=", apiKey, "&part=snippet,contentDetails"].join("");
	$.get( targetUrl, function( data ) {
	  console.log(data);
	  var channelID = data.items[0].id;
	  getChannelContent(channelID);

	});
};


var getChannelContent = function(channelID) {
	//1. get channel content
	var targetUrl = ["https://www.googleapis.com/youtube/v3/channels?id=" , channelID, 
						"&key=", apiKey, "&part=snippet,contentDetails"].join("");
	$.get( targetUrl, function( data ) {
	  console.log(data);
	  objChannel = data ;
	  parseChannel(data, channelID)

	});

};

var parseChannel = function(jsonChannel, channelID) {
	var title = jsonChannel.items[0].snippet.title ;
	$('#result').html(title);
	getBooks(channelID);
};

var selectedBook ;

var getBooks = function(channelID) {
	$('#divBooks').html('');
	//1. get channel content
	var targetUrl = ["https://www.googleapis.com/youtube/v3/channelSections?channelId=" , channelID, 
						"&key=", apiKey, "&part=snippet,contentDetails"].join("");
	$.get( targetUrl, function( data ) {
		books = data.items ;
		$.each(books, function(index, book) {
			$(["<div class='book'>", book.snippet.title, "</div>"].join("")).appendTo('#divBooks')
			.click(function() {
				$('div.book').removeClass('selected');
				$(this).addClass('selected');
				getPlayListContents(book);
			});
			//console.log(book.snippet.title);
			//console.log(book.contentDetails.playlists.join(","));
		});
	});
};

var playlists ;
var selectedPlaylist ;
var playlistIndex = 0;

//取得指定書本的 playlist 清單的內容
var getPlayListContents = function(book) {
	selectedBook = book ;
	$('#divPlaylists').html('');	//clear
	var playlistIDs = book.contentDetails.playlists.join(",") ;

	var targetUrl = ["https://www.googleapis.com/youtube/v3/playlists?id=" , playlistIDs, 
						"&key=", apiKey, "&part=snippet,contentDetails"].join("");
	$.get( targetUrl, function( data ) {
		playlists = data.items ;
		playlistIndex = 0;
		getVideos();

		$.each(playlists, function(index, playlist) {
			$(["<div class='playlist'>", playlist.snippet.title, "</div>"].join("")).appendTo('#divPlaylists')
			.click(function() {
				$('div.playlist').removeClass('selected');
				$(this).addClass('selected');
				console.log(playlist);
			});
			//console.log(book.snippet.title);
			//console.log(book.contentDetails.playlists.join(","));
		});
	});
};

var videos ;


var getVideos = function() {
	videos = [];
	$('#divVideos').html('loading ...');	//clear
	if (playlistIndex < playlists.length) {
		//取得指定 playlist id 的 videos 清單
		var playlist = playlists[playlistIndex];
		var targetUrl = ["https://www.googleapis.com/youtube/v3/playlistItems?playlistId=" , playlist.id, 
						"&key=", apiKey, "&part=contentDetails&fields=nextPageToken,pageInfo,items(id,contentDetails(videoId))&maxResults=50"].join("");
		$.get( targetUrl, function( data ) {
			var videoIds = data.items ;
			var videoidlist = [] ;
			$.each(videoIds, function(index, video) {
				videoidlist.push(video.contentDetails.videoId);
			});
			//取得指定 videos 的內容
			var strVideoIDs = videoidlist.join(",");

			var vidroUrl = ["https://www.googleapis.com/youtube/v3/videos?id=" , videoidlist.join(","), 
						"&key=", apiKey, "&part=snippet,contentDetails&fields=items(id,snippet(title,description),contentDetails(duration))&maxResults=50"].join("");
			$.get( vidroUrl, function( data ) {
				playlist.videos = data.items ;
				playlistIndex += 1;
				getVideos();
			});
		});
	}
	else {
		console.log("finished!") ;
		$('#divVideos').html('finished !! ');	//clear
		dumpContents();
	}
};


var dumpContents = function() {
	// var content = makeContent;
	// var uriContent = "data:application/octet-stream," + encodeURIComponent(content);
	// newWindow = window.open(uriContent, selectedBook.snippet.title);

	var data = makeBook();
	var json = JSON.stringify(data);
	var blob = new Blob([json], {type: "application/json"});
	var url  = URL.createObjectURL(blob);

	var a = document.createElement('a');
	var fileName = [objChannel.items[0].snippet.title ,selectedBook.snippet.title,  ".1kc"].join("");
	a.download    = fileName ;
	a.href        = url;
	a.textContent = "按此下載 " + fileName;
	document.getElementById('divDownload').appendChild(a);
	var br = document.createElement('br');
	document.getElementById('divDownload').appendChild(br);

};

//建立下載內容
var makeBook = function() {
	var rawTitle = objChannel.items[0].snippet.title ;
	var bookTitlePrefix = [rawTitle.substring(0,2), ':', rawTitle.substring(2,rawTitle.length)].join("");
	var bookName = [bookTitlePrefix ,selectedBook.snippet.title].join("");
	var book = { 
					"uqid":"",
					"name": bookName,
					"description":"",
					"logo":"data:image/png;base64,",
					"is_public":false,
					"chapters" : []
				}

	for(var i=0; i < playlists.length; i++) {
		var pl = playlists[i];
		var chap = {
						"uqid":"",
						"name": pl.snippet.title,
						"priority": (i+1),
						"units":[]
			};
		

		for(var j=0; j < pl.videos.length; j++) {
			var video = pl.videos[j];
			var duration = (moment.duration(video.contentDetails.duration) / 1000 ) + "" ;
			var unit = {
				"uqid":"",
				"name": video.snippet.title ,
				"priority": (j+1),
				"unit_type":"video",
				"content_url":"https://www.youtube.com/watch?v=" + video.id,
				"content_time": duration ,
				"supplementary_description": video.snippet.description ,
				"content":null,
				"completion":null,
				"optional":false,
				"is_preview":true,
				"logo":null,
				"quizzes":[]
			};
			chap.units.push(unit);
		}

		book.chapters.push(chap);
	}
	return book ;
}
